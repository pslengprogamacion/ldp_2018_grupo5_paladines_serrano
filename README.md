# Diseño de Automatas para la Evaluacion de Expresiones Regulares #

Proyecto Primer Parcial de la materia Lenguajes de Programación (SOFG1001) Paralelo 1 de la ESPOL.

### Descripción ###

* El proyecto busca crear una aplicación que facilite el entorno de diseño de autómatas de estado finito deterministas.  
* El Automata y sus métodos respectivos son implementados en Ruby.  
* Desde Java se llama a los método de Ruby para la presentación del autómata.  
* Se utiliza FXML para la interfaz gráfica.

### Integrantes ###

* Jorge Paladines
* Ricardo Serrano
