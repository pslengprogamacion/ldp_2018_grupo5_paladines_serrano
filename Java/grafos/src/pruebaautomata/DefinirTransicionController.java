/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebaautomata;

import org.espol.edu.ec.grafos.lista.Grafo;
import org.espol.edu.ec.ui.GrafoUI;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import org.jruby.embed.PathType;
import org.jruby.embed.ScriptingContainer;

/**
 * FXML Controller class
 *
 * @author rxsh96
 */
public class DefinirTransicionController implements Initializable {

    private int numEstados;
    private ArrayList<Integer> estadosAceptacion;
    private ArrayList<String> alfabeto;
    private ArrayList<TextField> listTextField;
    private ArrayList<String[]> transiciones;
    
    private Grafo<String> grafo;
    private GrafoUI<String> grafoUI;
    private ArrayList<String> estadosArray;
    
    @FXML
    private GridPane gridpane;
    @FXML
    private TextArea taEstados;
    @FXML
    private TextArea taAlfabeto;
    @FXML
    private Button bAtras;
    @FXML
    private Button bAceptar;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    public void load(int numEstados, ArrayList<Integer> estadosAceptacion, ArrayList<String> alfabeto){
        this.numEstados = numEstados;
        this.estadosAceptacion = estadosAceptacion;
        this.alfabeto = alfabeto;
        setGridPane(numEstados);
        setTaEstados();
        setTaAlfabeto();
    }
    
    private void setGridPane(int numEstados){
        listTextField = new ArrayList<>(); 
        for (int i = 0; i < numEstados; i++){
            Label label = new Label("Estado "+i);
            TextField tTransicion = new TextField();
            tTransicion.setPromptText("Ej: a,1|b,2|c,3");
            gridpane.add(label, 0, i);
            gridpane.add(tTransicion,1, i);
            listTextField.add(tTransicion);
        }
    }
    
    private void setTaEstados(){
        taEstados.appendText("Estados: \n");
        for (int i = 0; i < numEstados; i++) {
            taEstados.appendText("Estado "+i+"\n");
        }
        taEstados.appendText("Estados de Aceptación: \n");
        for (Integer estado : estadosAceptacion) {
            taEstados.appendText("Estado "+String.valueOf(estado)+"\n");
        }
    }
    
    private void setTaAlfabeto(){
        for (String string : alfabeto) {
            taAlfabeto.appendText(string+"\n");
        }
    }
    
    private void obtenerTransiciones(){
        transiciones = new ArrayList<>();
        ArrayList<String> element;
        for (TextField textField : listTextField) {
            //System.out.println((String)textField);
            element = new ArrayList<>();
            String[] split = textField.getText().split("\\|");
            transiciones.add(split);
        }
        System.out.println(transiciones);
    }
    
    public void aceptar(ActionEvent event){
        obtenerTransiciones();
        showAutomata();
    }
    
    //AQUI HAY QUE LLAMAR A RUBY PARA CREAR EL AUTOMATA
    public void showAutomata(){
        System.out.println(this.numEstados);
        System.out.println(this.alfabeto);
        
        grafo = new Grafo(true);
        
        ScriptingContainer container = new ScriptingContainer();
        String path = "../../Ruby/Automata.rb";
        List<String> loadPaths = new ArrayList();
        loadPaths.add(path);
        container.setLoadPaths(loadPaths);
        
        Object receiver = container.runScriptlet(PathType.RELATIVE, path);
        
        Object[] arr = {this.numEstados,this.alfabeto.toArray(),this.estadosAceptacion.toArray()};
        container.callMethod(receiver, "javaCrearEstadosYAlfabeto", arr, String.class);
        
        String[] idEstados = container.callMethod(receiver, "getIdEstados", String[].class);
        estadosArray = new ArrayList<>();
        estadosArray.addAll(Arrays.asList(idEstados));
        
        for(int a = 0; a < this.transiciones.size(); a++){
            String estadoActual = idEstados[a];
            System.out.println(this.transiciones.get(a));
            for(String tranCOMAS: this.transiciones.get(a)){
                if(tranCOMAS.contains(",")){
                    String estadoSiguiente = tranCOMAS.split(",")[1];
                    String alfabetoTRAN = tranCOMAS.split(",")[0];
                    String[] arg = {estadoActual,estadoSiguiente,alfabetoTRAN};
                    container.callMethod(receiver, "generarTrancicionesJava", arg, String.class);
                }
            }
        }
        
        String[] transiciones = container.callMethod(receiver, "getTransiciones", String[].class);
        ArrayList<String[]> transArray = new ArrayList<>();
        for(String t: transiciones){
            transArray.add(t.split(","));
        }
        
        String[] estadosAceptacion = container.callMethod(receiver, "listaEstAceptacion", String[].class);
        
        estadosArray.forEach((s) -> {
            grafo.agregarVertice(s);
        });
        for(String e: estadosAceptacion){
            grafo.buscarVertice(e).setAceptacion(true);
        }
        
        transArray.forEach((s) -> {
            grafo.agregarArco(s[0],s[1],s[2]);
        });
        
        grafoUI = new GrafoUI(grafo);
        grafoUI.mostrarGrafo();
        
    }    
    
    
    public void goBack(ActionEvent event){
        try {
            FXMLLoader loader = new FXMLLoader();
            AnchorPane root = (AnchorPane)loader.load(getClass().getResource("DefinirAlfabeto.fxml").openStream());
            DefinirAlfabetoController definirAlfabeto = (DefinirAlfabetoController)loader.getController();
            definirAlfabeto.load(numEstados, estadosAceptacion);
            Scene scene = new Scene(root);
            Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
            stage.setScene(scene);
            stage.setTitle("Automatas");
            stage.centerOnScreen();
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(IngresarExpresionController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
