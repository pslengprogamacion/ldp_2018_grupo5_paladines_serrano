/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebaautomata;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author rxsh96
 */
public class DefinirAlfabetoController implements Initializable {

    private int numEstados;
    private ArrayList<Integer> estadosAceptacion;
    private ArrayList<String> alfabeto;
    @FXML
    private TextField tAlfabeto;
    @FXML
    private Button bAceptar;
    @FXML
    private Button bAtras;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    public void load(int numEstados, ArrayList<Integer> estadosAceptacion){
        this.numEstados = numEstados;
        this.estadosAceptacion = estadosAceptacion;
        this.alfabeto = new ArrayList<>();
    }
    
    @FXML 
    public void aceptarAlfabeto(ActionEvent event){
        aceptarAlfabeto();
        try {
            FXMLLoader loader = new FXMLLoader();
            AnchorPane root = (AnchorPane)loader.load(getClass().getResource("DefinirTransicion.fxml").openStream());
            DefinirTransicionController transicion = (DefinirTransicionController)loader.getController();
            transicion.load(numEstados, estadosAceptacion, alfabeto);
            Scene scene = new Scene(root);
            Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
            stage.setScene(scene);
            stage.setTitle("Automatas");
            stage.centerOnScreen();
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(IngresarExpresionController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void aceptarAlfabeto(){
        for (char c : tAlfabeto.getText().toCharArray()) {
            String caracter = String.valueOf(c);
            if (!alfabeto.contains(caracter)) {
                alfabeto.add(String.valueOf(c));
            }
        }
        //System.out.println(alfabeto.toString());
    }
    
    @FXML
    public void goBack(ActionEvent event){
        try {
            FXMLLoader loader = new FXMLLoader();
            AnchorPane root = (AnchorPane)loader.load(getClass().getResource("EstadosAceptacion.fxml").openStream());
            EstadosAceptacionController estAceptacion = (EstadosAceptacionController)loader.getController();
            estAceptacion.load(numEstados);
            Scene scene = new Scene(root);
            Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
            stage.setScene(scene);
            stage.setTitle("Automatas");
            stage.centerOnScreen();
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(IngresarExpresionController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
