/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebaautomata;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author rxsh96
 */
public class IngresarTransicionController implements Initializable {

    @FXML
    private TextField tEstados;
    @FXML
    private Button bAtras;
    @FXML
    private Button bAceptar;
    
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    public void aceptarEstados(ActionEvent event){
        try {
            FXMLLoader loader = new FXMLLoader();
            AnchorPane root = (AnchorPane)loader.load(getClass().getResource("EstadosAceptacion.fxml").openStream());
            EstadosAceptacionController estAceptacion = (EstadosAceptacionController)loader.getController();
            estAceptacion.load(Integer.parseInt(tEstados.getText()));
            Scene scene = new Scene(root);
            Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
            stage.setScene(scene);
            stage.setTitle("Automatas");
            stage.centerOnScreen();
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(IngresarExpresionController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void goBack(ActionEvent event){
        try {
            Parent root = FXMLLoader.load(getClass().getResource("Menu.fxml"));
            Scene scene = new Scene(root);
            Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
            stage.setScene(scene);
            stage.setTitle("Automatas");
            stage.centerOnScreen();
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(IngresarTransicionController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
