/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebaautomata;

import org.espol.edu.ec.grafos.lista.Grafo;
import org.espol.edu.ec.ui.GrafoUI;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.jruby.embed.PathType;
import org.jruby.embed.ScriptingContainer;


/**
 * FXML Controller class
 *
 * @author rxsh96
 */

public class IngresarExpresionController implements Initializable {

    private Grafo<String> grafo;
    private GrafoUI<String> grafoUI;
    private ArrayList<String> estadosArray;
    
    @FXML 
    private TextField lExpresion;
    @FXML
    private TextField lCadena;
    @FXML
    private Button bAceptar;
    @FXML
    private Button bBack;
    @FXML 
    private Button bValidacion;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    @FXML
    public void showAutomata(){
        grafo = new Grafo(true);
        
        ScriptingContainer container = new ScriptingContainer();
        String path = "../../Ruby/Automata.rb";
        List<String> loadPaths = new ArrayList();
        loadPaths.add(path);
        container.setLoadPaths(loadPaths);
        
        Object receiver = container.runScriptlet(PathType.RELATIVE, path);
        
        container.callMethod(receiver, "crearAutExpReg", lExpresion.getText(), String[].class);
        
        String[] idEstados = container.callMethod(receiver, "getIdEstados", String[].class);
        estadosArray = new ArrayList<>();
        estadosArray.addAll(Arrays.asList(idEstados));
        
        String[] transiciones = container.callMethod(receiver, "getTransiciones", String[].class);
        ArrayList<String[]> transArray = new ArrayList<>();
        for(String t: transiciones){
            transArray.add(t.split(","));
        }
        
        String[] estadosAceptacion = container.callMethod(receiver, "listaEstAceptacion", String[].class);
        
        estadosArray.forEach((s) -> {
            grafo.agregarVertice(s);
        });
        for(String e: estadosAceptacion){
            grafo.buscarVertice(e).setAceptacion(true);
        }
        
        transArray.forEach((s) -> {
            grafo.agregarArco(s[0],s[1],s[2]);
        });
        
        grafoUI = new GrafoUI(grafo);
        grafoUI.mostrarGrafo();
        bValidacion.setDisable(false);
    }
    
    @FXML
    private void goBack(ActionEvent event){
        try {
            Parent root = FXMLLoader.load(getClass().getResource("Menu.fxml"));
            Scene scene = new Scene(root);
            Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
            stage.setScene(scene);
            stage.centerOnScreen();
            stage.setTitle("Automatas");
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(IngresarExpresionController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    @FXML
    private void validarCadenas(ActionEvent event){
        try {
            FXMLLoader loader = new FXMLLoader();
            AnchorPane root = (AnchorPane)loader.load(getClass().getResource("ValidarCadena.fxml").openStream());
            ValidarCadenaController vcc = (ValidarCadenaController)loader.getController();
            vcc.loadGrafos(grafo, grafoUI, estadosArray);
            Scene scene = new Scene(root);
            Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
            stage.setScene(scene);
            stage.setTitle("Automatas");
            stage.centerOnScreen();
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(IngresarExpresionController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
