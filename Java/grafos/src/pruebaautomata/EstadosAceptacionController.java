/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebaautomata;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author rxsh96
 */
public class EstadosAceptacionController implements Initializable {

    private int numEstados;
    private ArrayList<Integer> estadosAceptacion;
    private HashMap<Integer, CheckBox> listaCheck;
    @FXML
    private GridPane gridpane;
    @FXML
    private Button bAtras;
    @FXML
    private Button bAceptar;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    public void load(int numEstados){
        this.numEstados = numEstados;
        estadosAceptacion = new ArrayList<>();
        setEstados(numEstados);
    }
    
    private void setEstados(int numEstados){
        listaCheck = new HashMap<>();
        for (int i = 0; i < numEstados; i++){
            Label label = new Label("Estado "+i);
            CheckBox checkBox = new CheckBox();
            gridpane.add(label, 0, i);
            gridpane.add(checkBox,1, i);
            listaCheck.put(i, checkBox);
        }
    }
    
    @FXML
    private void addEstadosAceptacion(ActionEvent event){
        addEstadosAceptacion();
        try {
            FXMLLoader loader = new FXMLLoader();
            AnchorPane root = (AnchorPane)loader.load(getClass().getResource("DefinirAlfabeto.fxml").openStream());
            DefinirAlfabetoController definirAlfabeto = (DefinirAlfabetoController)loader.getController();
            definirAlfabeto.load(numEstados, estadosAceptacion);
            Scene scene = new Scene(root);
            Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
            stage.setScene(scene);
            stage.setTitle("Automatas");
            stage.centerOnScreen();
            stage.show();
            //System.out.println(estadosAceptacion.toString());
        } catch (IOException ex) {
            Logger.getLogger(IngresarExpresionController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    private void addEstadosAceptacion(){
        for (Integer i : listaCheck.keySet()) {
            if (listaCheck.get(i).isSelected() && !estadosAceptacion.contains(i)) {
                estadosAceptacion.add(i);
            }
            else if(!listaCheck.get(i).isSelected() && estadosAceptacion.contains(i)){
                estadosAceptacion.remove(i);
            }
        }
    }
    
    public void goBack(ActionEvent event){
        try {
            Parent root = FXMLLoader.load(getClass().getResource("IngresarTransicion.fxml"));
            Scene scene = new Scene(root);
            Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
            stage.setScene(scene);
            stage.centerOnScreen();
            stage.setTitle("Automatas");
            stage.show();
            System.out.println(estadosAceptacion.toString());
        } catch (IOException ex) {
            Logger.getLogger(IngresarExpresionController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
