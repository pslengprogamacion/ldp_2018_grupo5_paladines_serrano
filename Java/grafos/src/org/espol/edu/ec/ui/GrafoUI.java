package org.espol.edu.ec.ui;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.ListIterator;
import org.espol.edu.ec.grafos.lista.*;

// Estas librerias se encuentran en:
//     -->./lib/gs-core-1.3
//     -->./lib/gs-ui-1.3
import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;

/**
 *
 * @author markoscalderon
 */
public class GrafoUI<T> {
    private Graph graph;
    
    public GrafoUI(Grafo<T> g){
        System.setProperty("org.graphstream.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
        graph = new SingleGraph("Grafo");
        //-->aqui se enuentra el archivo de configuracion:  ./lib/styles.css
        //Para mas detalles ver:  http://graphstream-project.org/doc/Advanced-Concepts/GraphStream-CSS-Reference/
        graph.addAttribute("ui.stylesheet", "url(./lib/styles.css)");
        
        //dibujar los vertices
        for (Vertice<T> v: g.getVertices()) {
            graph.addNode(v.getContenido().toString()).addAttribute("ui.label", v.getContenido().toString());
            if(v.isAceptacion())
                graph.getNode(v.getContenido().toString()).addAttribute("ui.style","fill-color: yellow; text-alignment: center; shadow-mode: plain; shadow-width: 5; shadow-offset: 0; shadow-color: black;");
            else
                graph.getNode(v.getContenido().toString()).addAttribute("ui.style", "fill-color: yellow; text-alignment: center; shadow-mode: plain; shadow-width: 2; shadow-offset: 0; shadow-color: black;");
        }
        
        //buscar arcos del grafo
        for (Vertice<T> v1: g.getVertices()) {
            for (Vertice<T> v2: g.getVertices()) {
                Arco<T> arco = v1.buscarArco(v2);
                if(arco != null) {
                    String cv1 = v1.getContenido().toString();
                    String cv2 = v2.getContenido().toString();
                    if (!g.isDirigido()) {
                        if (graph.getNode(cv1).hasEdgeBetween(cv2)) {
                            continue;
                        }
                    }
                    //dibujar los arcos
                    Edge edge = graph.addEdge(cv1+cv2, cv1, cv2, g.isDirigido());
                    edge.addAttribute("ui.label", arco.getPeso());//mostrar etiqueta
                    if(cv1.equals(cv2))
                        edge.addAttribute("ui.style","text-offset: 35, -50; shape: cubic-curve; shape: freeplane;");
                    
                    /*if (arco.getPeso() != -1) {//si el arco tiene un peso asignado (No es -1) se cambia el formato
                        if(arco.getPeso() <10) {
                            //Si peso del arco es menor a 10 dibujarlo en amarillo
                            edge.addAttribute("ui.label", arco.getPeso()); //mostrar etiqueta
                            edge.addAttribute("ui.style", "fill-color: rgb(255,255,0);");//pintar arco amarillo
                        }
                        else {//Si peso del arco es mayor o igual a 10 dibujarlo  en azul
                            edge.addAttribute("ui.label", arco.getPeso());//mostrar etiqueta
                            edge.addAttribute("ui.style", "fill-color: rgb(0,0,255);");//pintar arco azul
                        }
                    }*/
                }
            }
        }
    }

    public void mostrarGrafo() {
        graph.display();
        try { Thread.sleep(1500); } catch (Exception e) {}
    }
    
    public void mostrarAceptacion(String[] estAcep){
        //ListIterator<T> k = vertices.listIterator();

        for(String s: estAcep){
            Node nodo = graph.getNode(s);
            nodo.addAttribute("ui.style","fill-color: yellow; text-alignment: center; shadow-mode: plain; shadow-width: 5; shadow-offset: 0; shadow-color: black;");
        }
        
        /*while (k.hasNext()) {
            T v = k.next();
            Node nodo = graph.getNode(v.toString());
            nodo.setAttribute("ui.class", "marked");
            try { Thread.sleep(1500); } catch (Exception e) {}
        }*/
    }
    
    public String mostrarRecorrido(String cadena, ArrayList<String> estados, Grafo<T> g){
        try { Thread.sleep(2000); } catch (Exception e) {}
        T idFinal = null;
        Vertice<T> v = g.getVertices().getFirst();
        for(char c: cadena.toCharArray()){
            Node nodo = graph.getNode(estados.indexOf(v.getContenido()));
            nodo.setAttribute("ui.style", "fill-color: red;");
            try { Thread.sleep(1000); } catch (Exception e) {}
            nodo.setAttribute("ui.style", "fill-color: yellow;");
            try { Thread.sleep(1000); } catch (Exception e) {}
            idFinal = v.getContenido();
            try{
                Arco<T> arco = null;
                for(Arco<T> a: v.getArcos()){
                    if(a.getPeso().equals(Character.toString(c))){
                        arco = a;
                        break;
                    }
                }
                if(arco != null){
                    v = g.buscarVertice(arco.getDestino().getContenido());
                }
            }
            catch(Exception e){
                return("NO ES UNA CADENA VÁLIDA");
                
            }
        }
        if(g.buscarVertice(idFinal).isAceptacion())    
            return("ES UNA CADENA VÁLIDA");
        else
            return("NO ES UNA CADENA VÁLIDA");
    }
}