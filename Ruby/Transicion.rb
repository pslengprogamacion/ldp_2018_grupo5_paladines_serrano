class Transicion
  
  def initialize(estPartida, estDestino, elAlfa)
    #El estado del que sale la flecha de
    @estPartida = estPartida
    
    #El estado al que llega la flecha
    @estDestino = estDestino
    
    #El elemento del alfabeto asignado a la transicion
    @elAlfa = elAlfa
  end
  
  #Getters y Setters
  def getPartida
    return @estPartida
  end
  
  def getDestino
    return @estDestino
  end
  
  def getAlfa
    return @elAlfa
  end
  
  def setPartida=(valor)
    @estPartida = valor
  end
  
  def setDestino=(valor)
    @estDestino = valor
  end
  
  def setAlfa=(valor)
    @elAlfa = valor
  end
  
  def to_s
    "#{@estPartida},#{@estDestino},#{@elAlfa}"
  end
  
end