require 'java'
import 'java.util.Arrays'
require '../../Ruby/Estado.rb'
require '../../Ruby/Transicion.rb'

class Automata
  
  def initialize()
    #Primero se inicializa una lista vacía de estados de aceptación
    @estAceptacion = []
    
    @estados = []
    #Después se crea el alfabeto
    @alfabeto = []
    #Luego las transiciones
    @transiciones = []
    #Finalmente se asigna el estado inicial

    #Símbolos por si se escoge crear desde expresión regular
    @simbolosMasYAst = ["+","*"]
    @simbolosGuion = ["|"]
  end

  #Metodo que establece el numero de estados que se van a crear
  def definirNumEstados()
    print "Cuantos estados posee el automata?: "
    numEstados = gets.to_i
    return numEstados
  end
  
  
  #Metodo que muestra todos los estados
  def showEstados(numEstados)
    print "Estos son los estados: "
    print "|"
    for i in 0..(numEstados-1)
      print "#{i}|"
    end
    puts ""
  end
  
  
  #Metodo que define que estado es de aceptacion
  def definirEstadosAceptacion(numEstados)
    print "Cuales de ellos desea que sean estados de aceptacion?(Separe por comas): "
    estAcp = gets.chomp
    eAceptacionAsignar = []
    for a in estAcp.split(",")
      eAceptacionAsignar << a.to_i
    end
    return eAceptacionAsignar
  end
  
  
  #Metodo que crea los Estados
  def crearEstados()
    numEstados = definirNumEstados()
    showEstados(numEstados)
    eAceptacionAsignar = definirEstadosAceptacion(numEstados)
    estados = []
    for idEstado in 0.. (numEstados-1)
      if eAceptacionAsignar.include?(idEstado)
        estadoAsignar = Estado.new(true, idEstado)
        estados << estadoAsignar
        @estAceptacion << estadoAsignar
      else 
        estados << Estado.new(false, idEstado)
      end
    end 
    return estados
  end
  
  
  #Método que crea el alfabeto a utilizar
  def crearAlfabeto()
    print "Escriba el alfabeto a utilizar, separado por comas: "
    alfabetoSeparadoPorComas = gets.chomp
    alfabeto = alfabetoSeparadoPorComas.split(",")
    puts "Alfabeto: #{alfabeto}"
    puts ""
    return alfabeto
  end

  
  def ingresoElementoEstado()
    listElementoEstado = []
    while (elementoEstado = gets) != "\n" do
      tran = elementoEstado.split(",")
      if tran.length != 2
        puts "Ha escrito mal la asignacion. Intente de nuevo."
      else
        if not @alfabeto.include?(tran[0])
          puts "Debe asignar un elemento del alfabeto definido en el paso anterior"
        elsif not Array(0..@estados.size()).include?(tran[1].to_i)
          puts "Debe asignar un estado correcto"
        else
          listElementoEstado << tran
        end
      end
    end
    return listElementoEstado 
  end
  
  
  def crearTransiciones()
    transiciones = []
    puts "Ahora debe asignar las transiciones. Recuerde que sólo puede existir una transicion para el estado por cada elemento del alfabeto"
    puts "Primero el elemento del alfabeto, seguido de una coma, y el estado al que se le asigna"
    puts "(presione ENTER sin escribir para terminar de asignar): "
    for estadoPartida in @estados
      puts "--- Estado #{estadoPartida.getIdEstado} ---"
      listElementoEstado = ingresoElementoEstado()
      if listElementoEstado != nil
        for tran in listElementoEstado
          elementoAlfa = tran[0]
          elementoDestino = tran[1].to_i
          transicion = Transicion.new(estadoPartida.to_s, elementoDestino, elementoAlfa)

          transiciones << transicion
          estadoPartida.agregarTransicion(transicion)
        end
      end
    end
    @transiciones = transiciones
  end
  
  def to_s
    for estado in @estados
      idEst = estado.getIdEstado
      puts estado
      for tran in estado.getTransiciones
        puts tran
      end
      puts ""
    end
  end
  
  ##########################
  ##########################
  ##########################

  def javaCrearEstadosYAlfabeto(numEstados,alfabeto,listEstAcep)
    @alfabeto = alfabeto
    @estAceptacion = listEstAcep
    for i in 0.. numEstados-1
      if listEstAcep.include?(i)
        aceptacion = true
      else
        aceptacion = false
      end
      @estados << Estado.new(aceptacion,i)
    end
    return " "
  end

  def generarTrancicionesJava(idEstadoPartida,idEstadoLlegada,elemAlfa)
    estadoActual = nil
    for e in @estados
      if e.getIdEstado == idEstadoPartida
        estadoActual = e
        break
      end
    end
    t = Transicion.new(idEstadoPartida,idEstadoLlegada,elemAlfa)
    @transiciones << t
    e.agregarTransicion(t)
    return " "
  end

  private def validarParentesis(expresion)
    contador = 0

    if expresion.include?("(") or expresion.include?(")")
      expresion.each_char do |e|
        if e == "("
          contador = contador + 1
        elsif e == ")"
          contador = contador - 1
        end
      end
      
      if (contador != 0)
        return false
      else
        return true
      end
    else
      return true
    end
  end

  private def validarExpresion(expresion)
    validadoAceptado = validarParentesis(expresion)
    if(validadoAceptado)
      for alfaID in 1.. (expresion.length-1)
        if (@simbolosMasYAst.include?(expresion[alfaID]) and (@simbolosMasYAst.include?(expresion[alfaID-1]) or @simbolosGuion.include?(expresion[alfaID-1])))
          puts "Error: No puede tener dos símbolos seguidos"
          return nil
        elsif (@simbolosMasYAst.include?(expresion[alfaID]) and (expresion[alfaID-1] == "(")) #RECORDAR CAMBIAR ESE OR PARA QUE LOS SIMBOLOS FUNCIONEN
          puts "Error: No puede tener un símbolo luego de un paréntesis"
          return nil
        elsif (@simbolosGuion.include?(expresion[alfaID]) and @simbolosGuion.include?(expresion[alfaID-1]))
          puts "Error: Dos signos | seguidos"
          return nil
        end
      end
      return expresion
    else
      puts "Número incorrecto de paréntesis"
      return nil
    end
  end

  def generarSubGrafo(estadoAnterior, expresion, aceptacion)
    #Ahora se itera desde 0 hasta la última posición de la expresión
    for i in 0.. (expresion.length-1)
      caracter = expresion[i]
      #Si el caracter no es un símbolo y no forma parte del alfabeto
      if not @alfabeto.include?(caracter) and not @simbolosMasYAst.include?(caracter)
        @alfabeto << caracter
      end
      #Si no es un símbolo solamente
      if not @simbolosMasYAst.include?(caracter)
        #Si aún no es la última expresión
        if i < expresion.length-1
          #Qué hacer si ve un + en la siguiente posición
          if expresion[i+1] == "+"
            @cont = @cont + 1
            if i+1 == expresion.length-1
              aceptacion = true
            end
            estadoNuevo = Estado.new(aceptacion,@cont)
            aceptacion = false
            @estados << estadoNuevo
            @transiciones << Transicion.new(estadoAnterior.getIdEstado,estadoNuevo.getIdEstado,caracter)
            @transiciones << Transicion.new(estadoNuevo.getIdEstado,estadoNuevo.getIdEstado,caracter)
            estadoAnterior = estadoNuevo

          #Qué hacer si ve un * en la siguiente posición
          elsif expresion[i+1] == "*"
            @transiciones << Transicion.new(estadoAnterior.getIdEstado,estadoAnterior.getIdEstado,caracter)
            if i+1 == (expresion.length-1)
              estadoAnterior.setAceptacion(true)
            end    

          #Qué hacer si el siguiente caracter es otra simple expresión
          else
            if caracter == "("
              puts "("
            else
              @cont = @cont + 1
              estadoNuevo = Estado.new(false,@cont)
              @estados << estadoNuevo
              @transiciones << Transicion.new(estadoAnterior.getIdEstado,estadoNuevo.getIdEstado,caracter)
              estadoAnterior = estadoNuevo      
            end
          end
        #Este es el else de si llegó al final. Aquí entra si i apunta a la última expresión que no es símbolo
        else 
          #puts caracter
          @cont = @cont + 1
          estadoNuevo = Estado.new(true,@cont)
          @estados << estadoNuevo
          @transiciones << Transicion.new(estadoAnterior.getIdEstado,estadoNuevo.getIdEstado,caracter)
        end
      end
    end
  end

  def crearAutExpReg(expresion)
    expresion = validarExpresion(expresion)
    @cont = 1
    if(expresion != nil)
      #Se crean los atributos de la clase
      aceptacion = false
      @alfabeto = []
      @estados = []
      @transiciones = []
      #Se coloca un estado inicial
      estadoAnterior = Estado.new(true,@cont)
      @estados << estadoAnterior

      listaDeExpAIterar = expresion.split('|')
      listaDeExpAIterar[-1] = listaDeExpAIterar[-1][0..listaDeExpAIterar[-1].length-1]
      #puts listaDeExpAIterar[-1]

      for subExpresion in listaDeExpAIterar
        subEstadoInicial = estadoAnterior
        aceptacion = false
        generarSubGrafo(subEstadoInicial,subExpresion,aceptacion)
      end  

    else
      puts "La expresión regular no es correcta"
    end
  end
  
  def getIdEstados()
    listaId = []
    if @estados != nil
      for estado in @estados
        listaId << estado.getIdEstado().to_s
      end
      return listaId.to_java(java.lang.String)
    end
  end
  
  def getTransiciones()
    listaTranJava = []
    for transicion in @transiciones
      listaTranJava << transicion.to_s
    end
    #puts listaTranJava
    return listaTranJava.to_java(java.lang.String)
  end
  
  def listaEstAceptacion()
    res = []
    for e in @estados
      if(e.getAceptacion)
        res << e.getIdEstado().to_s
      end
    end
    return res.to_java(java.lang.String)
  end

  private :crearEstados, :crearTransiciones, :crearAlfabeto
  
end


Automata.new()