class Estado
  
  #El Estado debe recibir un estado de aceptación, ya sea true o false, y su id
  #Si es true, significa que es un estado de aceptación
  #Si es false, quiere decir lo contrario
  def initialize(aceptacion, idEstado)
    @aceptacion = aceptacion
    @idEstado = idEstado
    @transiciones = []
  end

  #Getters y Setters
  def getAceptacion
    @aceptacion
  end
  
  def getIdEstado()
    @idEstado
  end
  
  def getTransiciones
    @transiciones
  end

  def setAceptacion(valor)
    @aceptacion = valor
  end
  
  def setIdEstado(valor)
    @idEstado = valor
  end
  
  def setTransiciones(valor)
    @transiciones = valor
  end
  
  def agregarTransicion(transicion)
    @transiciones << transicion
  end

  #El método to String
  def to_s
    "#{getIdEstado()}"
  end
  
end